# User application

## Context
This project contains two separate applications, one for the server side and other for the client. Both have been developed as different applications. Both are using docker compose and make for running them easily in the local environment. In a production environment this would require to have different configuration like other specific compose or docker files per depending each environment. As this is an exercise some simplifications has been done, for instance when updating the user password, any kind of verification using email or two factor it's being done to ensure account identity, so the password it's just being encrypted and updated in the database. Another example it's that the login endpoint it's only checking that the user credentials are valid and returning a random access token that it's not required in any other part of the application. Also the user id's are autoincremental integers, in a production environment it would be better to use a non sequential value as user identifier.

## How to run
- [Server application readme](./server/README.md)

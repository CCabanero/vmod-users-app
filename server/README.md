# Server side
This is the server side of the project.

## Stack
- The api is made using [hapi](https://hapi.dev/)
- The database that is storing the users is **MYSQL**
- For testing [lab](https://hapi.dev/module/lab) is used as a test runner and [code](https://hapi.dev/module/code/) as an assertion library

## Dependencies
- Docker and docker-compose
- Make (optional as you can run the make commands manually)

## Preparing the application

- Build the docker image: **make server-build**
- Prepare the application (This will create the table users in the database): **make server-prepare**

## Testing

To test the application run **make sever-test** or **make server-test-end2end** to run only the end2end suite

## Running the application

To run the application in foreground **make server-up** and in background **make sever-upd**.
The server will be ready at **http:\\\\localhost:3000** after the setup. 


## Postman collection

There is also included a [postman](https://www.postman.com/) collection [file](./vmod_user_server.postman_collection.json)  in the project  in order to run the request locally
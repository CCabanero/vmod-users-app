'use strict';

const mysql = require('mysql');

const createConnection = async () => {
  const connection = await mysql.createConnection({
    host: process.env.MYSQL_USERS_HOST,
    user: process.env.MYSQL_USERS_USERNAME,
    password: process.env.MYSQL_USERS_PASSWORD,
    port : process.env.MYSQL_USERS_PORT
  });

  return connection;
};

const createDatabase = async (connection) => {
  await runQuery(connection, 'CREATE DATABASE IF NOT EXISTS users');
  await runQuery(connection, 'USE users');
};

const createTable = async (connection) => {
  await runQuery(connection, 'CREATE TABLE IF NOT EXISTS users(id int NOT NULL AUTO_INCREMENT, firstName VARCHAR(30) NOT NULL, middleName VARCHAR(30) NOT NULL, lastName VARCHAR(30) NOT NULL, email VARCHAR(30) NOT NULL, password VARCHAR(255) NOT NULL, phone VARCHAR(30) NOT NULL, country VARCHAR(3) NOT NULL, zipCode VARCHAR(10), PRIMARY KEY (id))');
};

const runQuery = async (connection, query) => {
  return await new Promise((resolve, reject) => {
    process.stdout.write(`RUNNING query: ${query} ...`);
    connection.query(query, (error, results, fields) => {
      if(error){
        process.stderr.write('fail!\n');
        reject(error);
      }
      else {
        process.stdout.write('done!\n');
        resolve({ results, fields });
      }
    });
  });
};

(async () => {
  try {
    const connection = await createConnection();
    await createDatabase(connection);
    await createTable(connection);
    console.log('DONE');

    process.exit(0);
  }catch(error) {
    console.log(`Error running the script, message: ${error.message}`);
    process.exit(1);
  }
})();



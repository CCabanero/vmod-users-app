'use strict';

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const userControllerValidations = require('./controllers/users/validations');

const internalServerError = (h) => {
  return processError(h, 'internal_server_error', 'There was an error processing your request', 500);
};

const serviceError = (h, error) => {
  return processError(h, error.type, error.message, 422);
};

const unauthorizedError = (h) => {
  return processError(h, 'unauthorized', 'Invalid credentials', 401);
};

const processError = (h, type, message, statusCode) => {
    return processResponse(h, { errors: [{ type: type, message: message }] }, statusCode);
};

const processSuccessfulResponse = (h, body) => {
    return processResponse(h, body, 200);
};

const processValidationErrorResponsse = (h, body) => {
    return processResponse(h, body, 400);
};

const processResponse = (h, body, statusCode) => {
  return h
    .response(JSON.stringify(body))
    .type('application/json')
    .code(statusCode)
    .takeover();
};

exports.createServer = (services) => {
  const server = Hapi.server({
    port: process.env.SERVER_PORT,
    host: process.env.SERVER_HOST
  });

  server.route({
    method: 'POST',
    path: '/users',
    handler: async (request, h) => {
      let response;
      try {
        response = await services.users.createUser(request.payload);
      }catch(e){
        return internalServerError(h);
      }

      if(!response.success) {
        return serviceError(h, response.error);
      }

      return processSuccessfulResponse(h, response);
    },
    options: {
      validate: {
        options: {
          abortEarly: false
        },
        payload: userControllerValidations.createUserRules,
        failAction: (request, h, error) => {
          const mappedErrors = userControllerValidations.mapValidationError(error);
          const errorResponse = {
            errors: mappedErrors
          };

          return processValidationErrorResponsse (h, errorResponse);
        }
      }
    }
  });

  server.route({
    method: 'PATCH',
    path: '/users/{userId}',
    handler: async (request, h) => {
      const userId = request.params.userId;
      let updatedUser;
      try {
        updatedUser = await services.users.updateUser(userId, request.payload);
      }catch(e){
        return internalServerError(h);
      }

      return processSuccessfulResponse(h, updatedUser);
    },
    options: {
      validate: {
        options: {
          abortEarly: false
        },
        payload: userControllerValidations.updateUserRules,
        failAction: (request, h, error) => {
          const mappedErrors = userControllerValidations.mapValidationError(error);
          const errorResponse = {
            errors: mappedErrors
          };

          return processValidationErrorResponsse(h, errorResponse);
        }
      }
    }
  });

  server.route({
    method: 'DELETE',
    path: '/users/{userId}',
    handler: async (request, h) => {
      const userId = request.params.userId;
      let response;
      try {
        response = await services.users.deleteUser(userId);
      }catch(e){
        return internalServerError(h);
      }

      if(response.success){
        return processSuccessfulResponse(h, { success: true });
      }
      else {
        return serviceError(h, response.error);
      }
    },
    options: {
      validate: {
        options: {
          abortEarly: false
        },
        payload: userControllerValidations.deleteUserRules,
        failAction: (request, h, error) => {
          const mappedResponse = userControllerValidations.mapValidationError(error);
          return processValidationErrorResponsse(h, mappedResponse);
        }
      }
    }
  });

  server.route({
    method: 'POST',
    path: '/users/login',
    handler: async (request, h) => {
      let response;
      try {
        response = await services.users.findByEmailAndPassword(request.payload.email, request.payload.password);
      }catch(e){
        return internalServerError(h);
      }

      if(response.success){
        const user = response.data;
        const authenticationToken = services.authentication.generateTokenForUser(user.id);
        return processSuccessfulResponse(h, { success: true, token: authenticationToken });
      }
      else {
        if(response.error.type == 'not_found'){
          return unauthorizedError(h);
        }
        else {
          return internalServerError(h);
        }
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return 'Users api';
    }
  });

  return server;
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

process.on('SIGINT', function() {
  console.log("Caught interrupt signal");
  process.exit();
});

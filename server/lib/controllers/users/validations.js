const Joi = require('@hapi/joi');

const countryPattern = /^[a-zA-Z]{3}$/;
const passwordPattern = /^[a-zA-Z0-9]{3,30}$/;
const phonePattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
const zipCodePattern = /^[0-9]{5}([- /]?[0-9]{4})?$/

exports.createUserRules = Joi.object({
  firstName: Joi.string().required(),
  middleName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().pattern(passwordPattern).required(),
  country: Joi.string().pattern(countryPattern).required(),
  phone: Joi.string().pattern(phonePattern).required(),
  zipCode: Joi.string().pattern(zipCodePattern).required()
});

exports.updateUserRules = Joi.object({
  firstName: Joi.string(),
  middleName: Joi.string(),
  lastName: Joi.string(),
  email: Joi.string().email(),
  password: Joi.string().pattern(passwordPattern),
  country: Joi.string().pattern(countryPattern),
  phone: Joi.string().pattern(phonePattern),
  zipCode: Joi.string().pattern(zipCodePattern)
});

exports.deleteUserRules = Joi.object({
  id: Joi.string()
});

exports.mapValidationError = (error) => {
  const errorKeys = {
    'any.required': 'required',
    'string.base': 'invalid',
    'string.pattern.base': 'invalid',
    'string.email': 'invalid'
  };
  const mappedMessages = {
    'invalid-password': 'required at least 3 alphanumeric characters and 30 maximum',
    'invalid-country': 'is not a valid iso3 code',
    'invalid-phone': 'is not a valid phone number',
    'invalid-zipCode': 'invalid zip code number'
  };

  return error.details.map((detail) => {
    const type = errorKeys[detail.type];
    const message = mappedMessages[`${type}-${detail.context.key}`] || detail.message

    return {
      'type': type,
      'key': detail.context.key,
      'message': message
    }
  });
};

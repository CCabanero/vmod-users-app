const mysql = require('mysql');
const bcrypt = require('bcrypt');

class UserService {
  constructor(mysqlConfiguration){
    this.connection = createMysqlConnection(mysqlConfiguration);
  }

  async findByEmailAndPassword(email, password){
    const user = await findByEmail(this.connection, email);

    if(!user) return notFoundResponse();

    const areEqual = await bcrypt.compare(password, user.password);
    if(!areEqual) return notFoundResponse();

    return successfulResponse({
      id: user.id,
      firstName: user.firstName,
      middleName: user.middleName,
      lastName: user.lastName,
      email: user.email,
      phone: user.phone,
      country: user.country,
      zipCode: user.zipCode
    });
  }

  async createUser(payload){
    const userWithThatEmailFound = await findByEmail(this.connection, payload.email);

    if(userWithThatEmailFound) return emailAlreadyExistsResponse();

    const sql = "INSERT INTO users SET firstName = ?, middleName = ?, lastName = ?, email = ?, password = ?, phone = ?, country = ?, zipCode = ?";
    const hashedPassword = await hashPassword(payload.password);
    const parameters = [payload.firstName, payload.middleName, payload.lastName, payload.email, hashedPassword, payload.phone, payload.country, payload.zipCode];
    const response = await runQuery(this.connection, sql, parameters);
    const results = response.results;

    return successfulResponse({
      id: results.insertId,
      firstName: payload.firstName,
      middleName: payload.middleName,
      lastName: payload.lastName,
      email: payload.email,
      phone: payload.phone,
      country: payload.country,
      zipCode: payload.zipCode
    });
  }

  async updateUser(userId, payload){
    const queryComponents = await buildUpdateQuery(userId, payload);

    const response = await runQuery(this.connection, queryComponents.query, queryComponents.parameters);
    const results = response.results;
    if (results.affectedRows == 0) return invalidUserIdResponse();

    const updatedUser = await findById(this.connection, userId);

    return successfulResponse({
      id: updatedUser.id,
      firstName: updatedUser.firstName,
      middleName: updatedUser.middleName,
      lastName: updatedUser.lastName,
      email: updatedUser.email,
      phone: updatedUser.phone,
      country: updatedUser.country,
      zipCode: updatedUser.zipCode
    });
  }

  async deleteUser(userId){
    const query = 'DELETE FROM users WHERE id = ?';
    const parameters = [userId];

    const response = await runQuery(this.connection, query, parameters);

    const affectedRows = response.results.affectedRows;
    const success = affectedRows > 0;

    if(success){
      return { success }
    }else {
      return invalidUserIdResponse();
    }
  }
};

const hashPassword = async (plainPassword) => await bcrypt.hash(plainPassword, 5);

const findById = async(connection, userId) => {
  const query = "SELECT * from users WHERE id = ?";
  const params = [userId];

  const results = await runQuery(connection, query, params);
  return results.results[0];
};

const findByEmail = async(connection, email) => {
  const sql = "SELECT * FROM users WHERE email = ? LIMIT 1";
  const response = await runQuery(connection, sql, [email]);

  return response.results[0];
};

const buildUpdateQuery = async (userId, payload) => {
  const parameters = [];
  const base = "UPDATE users SET";
  const where = `WHERE id = ?`
  const keys = Object.keys(payload);
  const fieldsQuery = await Promise.all(keys.map(async (key, index) =>  {
    let value;
    if(key == 'password') {
      value = await hashPassword(payload[key])
    }else {
      value = payload[key];
    }

    parameters.push(value);

    return `${key} = ?`
  }));

  const joinedQuery = fieldsQuery.join(', ');
  parameters.push(userId);

  return { query: `${base} ${joinedQuery} ${where}`, parameters: parameters };
};

const createMysqlConnection = (configuration) => {
  return mysql.createConnection({
    host : configuration.host,
    port: configuration.port,
    user: configuration.user,
    password: configuration.password,
    database: configuration.database
  });
};

const runQuery = async(connection, query, parameters) => {
  return await new Promise((resolve, reject) => {
    connection.query(query, parameters, (error, results, fields) => {
      resolve({ error, results, fields });
    });
  });
};

const successfulResponse = (data) => {
  return {
    success: true,
    data
  }
};

const notFoundResponse = () => {
  return {
    success: false,
    error: {
      type: 'not_found',
      message: 'Not found'
    }
  }
};

const invalidUserIdResponse = () => {
  return {
    success: false,
    error: {
      type: 'invalid_user_id',
      message: 'Invalid user id'
    }
  }
};

const emailAlreadyExistsResponse = () => {
  return {
    success: false,
    error: {
      type: 'email_already_exists',
      message: 'The email is already in use'
    }
  };
};

exports.instantiate = (mysqlConfig) => {
  return new UserService(mysqlConfig);
};

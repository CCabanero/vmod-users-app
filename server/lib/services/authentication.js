'use strict';

const crypto = require('crypto');

class AuthenticationService {
  generateTokenForUser(userId){
    return crypto.randomBytes(16).toString('hex');
  }
}

exports.instantiate = () => new AuthenticationService();


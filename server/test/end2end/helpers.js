'use strict';

const mysql = require('mysql');

const runQuery = async (connection, query) => {
  return await new Promise((resolve, reject) => {
    connection.query(query, (error, results, fields) => {
      if(error){
        reject(error);
      }
      else {
        resolve({ results, fields });
      }
    });
  });
};

exports.clearUsers = async () => {
  const connection = await mysql.createConnection({
    host: process.env.MYSQL_USERS_HOST,
    user: process.env.MYSQL_USERS_USERNAME,
    password: process.env.MYSQL_USERS_PASSWORD,
    port : process.env.MYSQL_USERS_PORT,
    database: process.env.MYSQL_USERS_DATABASE
  });

  await runQuery(connection, 'TRUNCATE table users');
};

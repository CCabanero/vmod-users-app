'use strict';

const Lab = require('@hapi/lab');
const got = require('got');
const { expect } = require('@hapi/code');
const { afterEach, before, beforeEach, describe, it, lab, suite } = exports.lab = Lab.script();
const helpers = require('./helpers');
require('../../index');

suite('End2End tests', () => {
  beforeEach(async () => {
    await helpers.clearUsers();
  });

  it('User creation', async () => {
    const body = {
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'SQPR',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };

    const response = await makeRequest('users', 'post', body);

    const expectedResponse =  {
      "success":true,
      "data": {
        "id":1,
        "firstName":"Cayo",
        "middleName":"Julio",
        "lastName":"Cesar",
        "email":"cjs@rome.com",
        "phone":"+34656789267",
        "country":"ITA",
        "zipCode":"12345-1234"
      }
    };
    expect(JSON.parse(response.body)).to.equal(expectedResponse);
  });

  it('User update', async () => {
    const creationBody = {
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'SQPR',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };
    const createdResponse = await makeRequest('users', 'post', creationBody);
    const createdUser = JSON.parse(createdResponse.body).data;

    const updateBody = { lastName: 'Cesareo' };
    const updateResponse = await makeRequest(`users/${createdUser.id}`, 'patch', updateBody);

    const expectedResponse = {
      success: true,
      data: {
        id: 1,
        firstName: 'Cayo',
        middleName: 'Julio',
        lastName: 'Cesareo',
        email: 'cjs@rome.com',
        phone: '+34656789267',
        country: 'ITA',
        zipCode: '12345-1234'
      }
    };
    expect(JSON.parse(updateResponse.body)).to.equal(expectedResponse);
  });

  it('User deletion', async() => {
    const creationBody = {
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'SQPR',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };
    const createdResponse = await makeRequest('users', 'post', creationBody);
    const userId = JSON.parse(createdResponse.body).data.id;

    const deleteResponse = await makeRequest(`users/${userId}`, 'delete', {});

    const expectedResponse = { success: true };
    expect(JSON.parse(deleteResponse.body)).to.equal(expectedResponse);
  });

  it('User login', async() => {
    const creationBody = {
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'SQPR',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };
    const createdResponse = await makeRequest('users', 'post', creationBody);

    const loginPayload = { email: creationBody.email, password: creationBody.password };
    const loginResponse = await makeRequest('users/login', 'post', loginPayload);

    const response = JSON.parse(loginResponse.body);
    expect(response.success).to.equal(true);
    expect(response.token).to.not.be.null();
  });

  const makeRequest = async (endpointUrl, method, payload) => {
    const baseUrl = `http://${process.env.SERVER_HOST}:${process.env.SERVER_PORT}`;
    const url = `${baseUrl}/${endpointUrl}`

    return await got[method](url,
      {
        json: payload
      },
      {
        responseType: 'json'
      }
    );
  };
});

'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { fail, afterEach, before, beforeEach, after, describe, it, lab, suite } = exports.lab = Lab.script();
const usersServiceFactory = require('../../lib/services/users');
const sinon = require('sinon');
const mysql = require('mysql');
const {createConnection} = mysql;
const bcrypt = require('bcrypt');

describe('Users service', () => {
  let userService;
  let mysqlConnection = { query: () => {} };
  let queryStub;

  beforeEach(() => {
    sinon.stub(mysql, 'createConnection').returns(mysqlConnection);
    userService = usersServiceFactory.instantiate({});
  });

  afterEach(() => {
    mysql.createConnection.restore();
  });

  describe('.create', () => {
    let findEmailResponse;
    let createUserResponse;

    beforeEach(() => {
      sinon.stub(bcrypt, 'hash').returns('hashedPassword');
      queryStub = sinon.stub(mysqlConnection, 'query').callsFake((query, parameters, callback) => {
        if(query.startsWith('SELECT')){
          callback(null, findEmailResponse, null);
        }else {
          callback(null, createUserResponse, null);
        }
      });
    });

    afterEach(() => {
      queryStub.restore();
      bcrypt.hash.restore();
    });

    const payload = {
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'SQPR',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };

    suite('when the email already exists', () => {
      before(() => {
        findEmailResponse = [{ id: 1, email: 'cjs@rome.com' }];
      });

      it('returns an error', async () => {
        const response = await userService.createUser(payload);

        const expectedResponse = {
          success: false,
          error: {
            type: 'email_already_exists',
            message: 'The email is already in use'
          }
        };
        expect(response).to.equal(expectedResponse);
      });
    });

    suite('when the emails does not exists', () => {
      before(() => {
        findEmailResponse = [];
        createUserResponse = { insertId : 1 }
      });

      it('creates an user in the database', async() => {
        const query = "INSERT INTO users SET firstName = ?, middleName = ?, lastName = ?, email = ?, password = ?, phone = ?, country = ?, zipCode = ?";
        const parameters = [payload.firstName, payload.middleName, payload.lastName, payload.email, 'hashedPassword', payload.phone, payload.country, payload.zipCode];

        await userService.createUser(payload);

        expect(queryStub.calledWith(query, parameters)).to.equal(true);
      });

      it('returns the user information', async() => {
        const payload = {
          firstName: 'Cayo',
          middleName: 'Julio',
          lastName: 'Cesar',
          email: 'cjs@rome.com',
          password: 'SQPR',
          phone: '+34656789267',
          country: 'ITA',
          zipCode: '12345-1234'
        };
        const query = "INSERT INTO users SET firstName = ?, middleName = ?, lastName = ?, email = ?, password = ?, phone = ?, country = ?, zipCode = ?";
        const parameters = [payload.firstName, payload.middleName, payload.lastName, payload.email, 'hashedPassword', payload.phone, payload.country, payload.zipCode];

        const response = await userService.createUser(payload);

        const expectedResponse = {
          success: true,
          data: {
            id: 1,
            firstName: payload.firstName,
            middleName: payload.middleName,
            lastName: payload.lastName,
            email: payload.email,
            phone: payload.phone,
            country: payload.country,
            zipCode: payload.zipCode
          }
        };
        expect(response).to.equal(expectedResponse);
      });
    });
  });

  describe('.findByUserAndPassword', () => {

    let queryStub;
    let findResponse = [];
    const email = 'email@domain.com';
    const password = 'aPassword';
    const payload = { email, password };
    
    beforeEach(() => {
      queryStub = sinon.stub(mysqlConnection, 'query').callsFake((query, parameters, callback) => {
        callback(null, findResponse, null);
      });
    });

    afterEach(() => {
      queryStub.restore();
    });

    suite('when the email is not valid', () => {
      before(() => {
        findResponse = [];
      });

      it('returns null', async() => {
        const response = await userService.findByEmailAndPassword(email, password);

        expect(response).to.equal({
          success: false,
          error: {
            type: 'not_found',
            message: 'Not found'
          }
        });
      });
    }); 

    suite('when the email is valid', () => {
      suite('but the password is not', () => {
        before(() => {
          const hashedPassword = bcrypt.hashSync('anotherPassword', 5);
          findResponse = [{ password: hashedPassword }];
        });

        it('returns a not found response', async() => {
          const response = await userService.findByEmailAndPassword(email, password);
          expect(response).to.equal({
            success: false,
            error: {
              type: 'not_found',
              message: 'Not found'
            }
          });
        });
      });

      suite('and the password too', () => {
        let hashedPassword;
        const user = {
          id: 1,
          firstName: 'Cayo',
          middleName: 'Julio',
          lastName: 'Cesar',
          email: 'cjs@rome.com',
          password: 'SQPR',
          phone: '+34656789267',
          country: 'ITA',
          zipCode: '12345-1234'
        };

        before(() => {
          hashedPassword = bcrypt.hashSync(password, 5);
          findResponse = [{...user, ...{password: hashedPassword } }]
        });

        it('returns the found user information', async() => {
          const response = await userService.findByEmailAndPassword(email, password);
          expect(response).to.equal({
            success: true,
            data: {
              id: user.id,
              firstName: user.firstName,
              middleName: user.middleName,
              lastName: user.lastName,
              email: user.email,
              phone: user.phone,
              country: user.country,
              zipCode: user.zipCode
            }
          });
        });
      });
    }); 
  });

  describe('.updateUser', () => {
    let queryStub;
    let findResponse = [];
    let affectedRows = 0;
    const userId = 1;
    const hashedPasswordValue = 'hashedPassword'
    const user = {
      id: 1,
      firstName: 'Cayo',
      middleName: 'Julio',
      lastName: 'Cesar',
      email: 'cjs@rome.com',
      password: 'hashedPassword',
      phone: '+34656789267',
      country: 'ITA',
      zipCode: '12345-1234'
    };
    
    beforeEach(() => {
      sinon.stub(bcrypt, 'hash').returns(hashedPasswordValue);
      queryStub = sinon.stub(mysqlConnection, 'query').callsFake((query, parameters, callback) => {
        const results = query.startsWith('SELECT') ? findResponse : { affectedRows };
        callback(null, results, null);
      });
    });

    afterEach(() => {
      queryStub.restore();
      bcrypt.hash.restore();
    });

    suite('when the received userId is not valid', () => {
      before(() => {
        findResponse = [];
        affectedRows = 0;
      });

      it('returns null', async() => {
        const payload = { firstName: 'New name', country: 'ESP' };

        const response = await userService.updateUser(userId, payload);

        expect(response).to.equal({
          success: false,
          error: {
            type: 'invalid_user_id',
            message: 'Invalid user id'
          }
        });
      });
    });

    suite('when the received userId is valid', () => {
      before(() => {
        findResponse = [user];
        affectedRows = 1;
      });

      it('updates an user in the database', async() => {
        const payload = { firstName: 'New name', country: 'ESP' };

        await userService.updateUser(userId, payload);

        const expectedQuery = "UPDATE users SET firstName = ?, country = ? WHERE id = ?";
        const expectedParams = [payload.firstName, payload.country, userId];

        expect(queryStub.calledWith(expectedQuery, expectedParams)).to.equal(true);
      });

      suite('when one of the updated fields is the password', () => {
        before(() => {
          findResponse = [user];
          affectedRows = 1;
        });

        it('updates it hashing it before', async () => {
          const payload = { password: 'A new password' };

          await userService.updateUser(userId, payload);

          const expectedQuery = "UPDATE users SET password = ? WHERE id = ?";
          const expectedParams = [hashedPasswordValue, userId];
          expect(queryStub.calledWith(expectedQuery, expectedParams)).to.equal(true);
        });
      });

      it('returns the user information', async() => {
        const payload = { password: 'A new password' };

        const response = await userService.updateUser(userId, payload);

        expect(response).to.equal({
          success: true,
          data: {
            id: user.id,
            firstName: user.firstName,
            middleName: user.middleName,
            lastName: user.lastName,
            email: user.email,
            phone: user.phone,
            country: user.country,
            zipCode: user.zipCode
          }
        });
      });
    });
  });

  suite('.deteUser', () => {
    let affectedRows = {};
    const userId = 1;

    beforeEach(() => {
      queryStub = sinon.stub(mysqlConnection, 'query').callsFake((query, parameters, callback) => {
        callback(null, { affectedRows }, null);
      });
    });

    afterEach(() => {
      queryStub.restore();
    });

    it('deletes the user from the database', async() => {
      await userService.deleteUser(userId);

      const expectedQuery = "DELETE FROM users WHERE id = ?";
      const expectedParams = [userId];

      expect(queryStub.calledWith(expectedQuery, expectedParams)).to.equal(true);

    });

    suite('when the received userId is not valid', () => {
      before(() => {
        affectedRows = 0;
      });

      it('returns a successfully response', async() => {
        const response = await userService.deleteUser(userId);

        expect(response).to.equal({
          success: false,
          error: {
            type: 'invalid_user_id',
            message: 'Invalid user id'
          }
        });
      });
    });

    suite('when the received userId is valid', () => {
      before(() => {
        affectedRows = 1;
      });

      it('returns an unsuccessfully response', async() => {
        const response = await userService.deleteUser(userId);

        expect(response).to.equal({ success: true });
      });
    });
  });
});


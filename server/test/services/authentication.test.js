'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, before, after, beforeEach, describe, it, lab, suite } = exports.lab = Lab.script();
const sinon = require('sinon');
const crypto = require('crypto');
const buffer = Buffer.alloc(16)
const authenticationServiceFactory = require('../../lib/services/authentication');
let cryptoStub;

describe('Authentication service', () => {
  let authenticationService;
  describe('.generateToken', () => {
    before(() => {
      cryptoStub = sinon.stub(crypto, 'randomBytes').callsFake((count) => buffer);
      authenticationService = authenticationServiceFactory.instantiate();
    });

    after(() => {
      cryptoStub.restore();
    });

    it('generates a token for an user id', () => {
      const response = authenticationService.generateTokenForUser('an-id');

      expect(response).to.equal('00000000000000000000000000000000');
    });
  });
});


'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, before, beforeEach, describe, it, lab, suite } = exports.lab = Lab.script();
const { createServer } = require('../../lib/server');
const sinon = require('sinon');

describe('User endpoints', () => {
  let server;
  let findByIdEmailAndPasswordMethod;
  let createUserMethod;
  let updateServiceMethod;
  let deleteUserServiceResponse;
  let userService = {};
  let authenticationService;
  const userId = 'an-id'

  beforeEach(async () => {
    userService = {
      findByEmailAndPassword: findByIdEmailAndPasswordMethod,
      createUser: createUserMethod,
      updateUser: updateServiceMethod,
      deleteUser: deleteUserServiceResponse
    };

    authenticationService = {
      generateTokenForUser: sinon.fake.returns('an-authorization-token')
    };

    server = createServer({
      users: userService,
      authentication: authenticationService
    });

    await server.initialize();
  });

  afterEach(async () => {
    await server.stop();
  });

  const makeRequest = async (method, url, payload = {}) => {
    return await server.inject({
      method: method,
      url: url,
      payload: payload
    });
  };

  describe('/users', () => {
    const validateIsAString = (fieldName, receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': fieldName,
        'message': `"${fieldName}" must be a string`
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    const validateIsValidEmail = (fieldName, receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': fieldName,
        'message': `"${fieldName}" must be a valid email`
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    const expectPasswordValidationError = (receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': 'password',
        'message': 'required at least 3 alphanumeric characters and 30 maximum'
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    const expectCountryValidationError = (receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': 'country',
        'message': 'is not a valid iso3 code'
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    const expectPhoneValidationError = (receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': 'phone',
        'message': 'is not a valid phone number'
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    const expectZipCodeValidationError = (receivedResponse) => {
      const expectedValidation = {
        'type': 'invalid',
        'key': 'zipCode',
        'message': 'invalid zip code number'
      };
      expect(receivedResponse.errors).to.include(expectedValidation);
    };

    describe('POST', () => {
      const validatePresence = (fieldName, receivedResponse) => {
        const expectedValidation = {
          'type': 'required',
          'key': fieldName,
          'message': `"${fieldName}" is required`
        };

        expect(receivedResponse.errors).to.include(expectedValidation);
      };

      suite('when some parameter is not valid', () => {
        suite('like the firstName', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('firstName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a string', () => {
            it("returns an error response", async () => {
              const payload = { firstName: 1 };

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('firstName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the middleName', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('middleName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a string', () => {
            it("return an error response", async () => {
              const payload = {
                middleName: 1
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('middleName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the lastName', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('lastName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a string', () => {
            it("return an error response", async () => {
              const payload = {
                lastName: 1
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('lastName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the email', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('email', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a valid email address', () => {
            it("return an error response", async () => {
              const payload = {
                email: 'invalid-email-address'
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsValidEmail('email', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the password', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('password', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a valid password', () => {
            it("return an error response", async () => {
              const payload = {
                password: 'a'
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              expectPasswordValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the country', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('country', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a valid iso3 code', () => {
            it("return an error response", async () => {
              const payload = {
                country: 'Spain'
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              expectCountryValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the phone', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('phone', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a valid phone', () => {
            it("return an error response", async () => {
              const payload = {
                phone: 123
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              expectPhoneValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the zip code', () => {
          suite('because it is missing', () => {
            it("return an error response", async () => {
              const payload = {};

              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              validatePresence('zipCode', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });

          suite('because it is not a valid format', () => {
            it("return an error response", async () => {
              const payload = {
                zipCode: 'invalid_zip_code'
              };
              const response = await makeRequest('post', '/users', payload);

              const receivedResponse = JSON.parse(response.payload);
              expectZipCodeValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });
      });

      suite('When all the received params are valid', () => {
        const payload = {
          firstName: 'Cayo',
          middleName: 'Julio',
          lastName: 'Cesar',
          email: 'cjs@rome.com',
          password: 'SQPR',
          phone: '+34656789267',
          country: 'ITA',
          zipCode: '12345-1234'
        };

        suite('when the server response is successful', () => {
          before(() => {
            createUserMethod = sinon.fake.returns({
              success: true,
              data: {
                id: userId,
                firstName: payload.firstName,
                middleName: payload.middleName,
                lastName: payload.lastName,
                email: payload.email,
                phone: payload.phone,
                country: payload.country,
                zipCode: payload.zipCode
              }
            })
          });

          it('calls the service to return the response', async () => {
            const response = await makeRequest('post', '/users', payload)

            expect(userService.createUser.calledWith(payload)).to.equal(true);
          });

          it('responds with the created user comming from the service', async () => {
            const response = await makeRequest('post', '/users', payload)

            const expectedResponse = {
              success: true,
              data: {
                id: userId,
                firstName: 'Cayo',
                middleName: 'Julio',
                lastName: 'Cesar',
                email: 'cjs@rome.com',
                phone: '+34656789267',
                country: 'ITA',
                zipCode: '12345-1234'
              }
            };
            expect(response.statusCode).to.equal(200);
            expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          });
        });

        suite('when the service response is not successful', () => {
          before(() => {
            createUserMethod = sinon.fake.returns({
              success: false,
              error: {
                type: 'email_already_exists',
                message: 'The email is already in use'
              }
            });
          });

          it('responds with the service error', async () => {
            const response = await makeRequest('post', '/users', payload)

            const expectedResponse = { errors: [{ type: 'email_already_exists', message: 'The email is already in use' }] };
            expect(JSON.parse(response.payload)).to.equal(expectedResponse);
            expect(response.statusCode).to.equal(422);
          });
        });

        suite('when the service throws an error', () => {
          before(() => {
            createUserMethod = sinon.fake.throws('An error');
          });

          it('returns an error', async () => {
            const response = await makeRequest('post', '/users', payload)

            const expectedResponse = { errors: [{ type: 'internal_server_error', message: 'There was an error processing your request' }] };
            expect(JSON.parse(response.payload)).to.equal(expectedResponse);
            expect(response.statusCode).to.equal(500);
          });
        });
      });
    });

    describe('PATCH', () => {
      suite('when some parameter is not valid', () => {
        suite('like the firstName', () => {
          suite('because it is not a string', () => {
            it("returns an error response", async () => {
              const payload = { firstName: 1 };

              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('firstName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the middleName', () => {
          suite('because it is not a string', () => {
            it("return an error response", async () => {
              const payload = {
                middleName: 1
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('middleName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the lastName', () => {
          suite('because it is not a string', () => {
            it("return an error response", async () => {
              const payload = {
                lastName: 1
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsAString('lastName', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the email', () => {
          suite('because it is not a valid email address', () => {
            it("return an error response", async () => {
              const payload = {
                email: 'invalid-email-address'
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              validateIsValidEmail('email', receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the password', () => {
          suite('because it is not a valid format', () => {
            it("return an error response", async () => {
              const payload = {
                password: 'a'
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              expectPasswordValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the country', () => {
          suite('because it is not a valid iso3 code', () => {
            it("return an error response", async () => {
              const payload = {
                country: 'Spain'
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              expectCountryValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the phone', () => {
          suite('because it is not a valid phone', () => {
            it("return an error response", async () => {
              const payload = {
                phone: 123
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              expectPhoneValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400)
            });
          });
        });

        suite('like the zipCode', () => {
          suite('because it is not a valid format', () => {
            it("return an error response", async () => {
              const payload = {
                zipCode: 'invalid_zip_code'
              };
              const response = await makeRequest('patch', `/users/${userId}`, payload);

              const receivedResponse = JSON.parse(response.payload);
              expectZipCodeValidationError(receivedResponse);
              expect(response.statusCode).to.equal(400);
            });
          });
        });
      });

      suite('when all the parameters are correct', () => {
        const updatePayload = {
          firstName: 'NewName',
          email: 'new-email@domain.com'
        };

        before(() => {
          updateServiceMethod = sinon.fake.returns({
            sucess: true,
            data: {
              id: userId,
              firstName: updatePayload.firstName,
              email: updatePayload.email,
              middleName: 'Julio',
              lastName: 'Cesar',
              phone: '+34656789267',
              country: 'ITA',
              zipCode: '12345-1234'
            }
          });
        });

        it('allow partial updates', async () => {
          const response = await makeRequest('patch', `/users/${userId}`, updatePayload);

          expect(userService.updateUser.calledWith(userId, updatePayload)).to.equal(true);
        });

        suite('when the update was successfully', () =>{
          it('responds with the created user comming from the service', async () => {
            const response = await makeRequest('patch', `/users/${userId}`, updatePayload)

            const expectedResponse = {
              sucess: true,
              data: {
                id: userId,
                firstName: updatePayload.firstName,
                middleName: 'Julio',
                lastName: 'Cesar',
                email: updatePayload.email,
                phone: '+34656789267',
                country: 'ITA',
                zipCode: '12345-1234'
              }
            };
            expect(response.statusCode).to.equal(200);
            expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          });
        });

        suite('when the update was not successfully', () =>{
          before(() => {
            updateServiceMethod = sinon.fake.returns({
              sucess: false,
            });

            it('returns an error response', async() => {
              const payload = { id: 'no-valid-id' };

              const response = await makeRequest('patch', `/users/${userId}`, updatePayload);

              const expectedResponse = { errors: [{ type: 'unprocessable_entity', message: 'An error' }] };
              expect(JSON.parse(response.payload)).to.equal(expectedResponse);
              expect(response.statusCode).to.equal(422);
            });
          });
        });

        suite('when the service throws an error', () => {
          before(() => {
            updateServiceMethod = sinon.fake.throws('An error');
          });

          it('returns an error', async () => {
            const payload = { id: 'no-valid-id' };

            const response = await makeRequest('patch', `/users/${userId}`, updatePayload);

            const expectedResponse = { errors: [{ type: 'internal_server_error', message: 'There was an error processing your request' }] };
            expect(JSON.parse(response.payload)).to.equal(expectedResponse);
            expect(response.statusCode).to.equal(500);
          });
        });
      });
    });

    suite('DELETE', () => {
      suite('when service responds successfully', () => {
        before(() => {
          deleteUserServiceResponse = sinon.fake.returns({ success: true });
        });

        it('returns a successful response', async () => {
          const response = await makeRequest('delete', '/users/non-valid-id');

          const expectedResponse = { success: true }
          expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          expect(response.statusCode).to.equal(200);
        });
      });

      suite('when service response was not successful', () => {
        before(() => {
          deleteUserServiceResponse = sinon.fake.returns({ success: false, error: { type: 'unprocessable_entity', message: 'An error' } });
        });

        it('returns an error', async () => {
          const response = await makeRequest('delete', '/users/valid-id');

          const expectedResponse = { errors: [{ type: 'unprocessable_entity', message: 'An error' }] };
          expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          expect(response.statusCode).to.equal(422);
        });
      });

      suite('when the service throws an error', () => {
        before(() => {
          deleteUserServiceResponse = sinon.fake.throws('Invalid id');
        });

        it('returns an error', async () => {
          const response = await makeRequest('delete', '/users/an-id');

          const expectedResponse = { errors: [{ type: 'internal_server_error', message: 'There was an error processing your request' }] };
          expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          expect(response.statusCode).to.equal(500);
        });
      });
    });
  });

  describe('/login', () => {
    suite('when the service returns an unknown error', () => {
      before(() => {
        findByIdEmailAndPasswordMethod  = () => {
          return {
            success: false,
            error: {
              type: 'a_error_type',
              message: 'An error happened'
            }
          }
        }
      });

      it('returns an internal server error', async () => {
        const payload = { email: 'email@domain.com', password: '123' }

        const response = await makeRequest('post', '/users/login', payload);

        const expectedResponse = {
          errors: [
            { type: 'internal_server_error', message: 'There was an error processing your request' }
          ]
        };
        expect(JSON.parse(response.payload)).to.equal(expectedResponse);
        expect(response.statusCode).to.equal(500);
      });
    });

    suite('when the credentials are incorrect', () => {
      before(() => {
        findByIdEmailAndPasswordMethod  = () => {
          return {
            success: false,
            error: {
              type: 'not_found',
              message: 'Not found'
            }
          }
        }
      });

      it('returns a not found error', async () => {
        const payload = { email: 'email@domain.com', password: '123' }

        const response = await makeRequest('post', '/users/login', payload);

        const expectedResponse = {
          errors: [
            { type: 'unauthorized', message: 'Invalid credentials' }
          ]
        };
        expect(JSON.parse(response.payload)).to.equal(expectedResponse);
        expect(response.statusCode).to.equal(401);
      });
    });

    suite('when the credentials correct', () => {
      before(() => {
        findByIdEmailAndPasswordMethod = () => {
          return {
            success: true,
            data: {
              id: userId,
              firstName: 'Cayo',
              middleName: 'Julio',
              lastName: 'Cesar',
              email: 'cjs@rome.com',
              password: 'SQPR',
              phone: '+34656789267',
              country: 'ITA',
              zipCode: '12345-1234'
            }
          }
        }
      });

      it('returns a successful response', async () => {
        const payload = { email: 'email@domain.com', password: '123' }

        const response = await makeRequest('post', '/users/login', payload);

        const expectedResponse = { success: true, token: 'an-authorization-token' }
        expect(JSON.parse(response.payload)).to.equal(expectedResponse);
        expect(response.statusCode).to.equal(200);
      });


      it('uses the authentication service to retrieve the token', async() => {
        const payload = { email: 'email@domain.com', password: '123' }

        await makeRequest('post', '/users/login', payload);

        expect(authenticationService.generateTokenForUser.calledWith(userId)).to.equal(true);
      });

      suite('when the service throws an error', () => {
        before(() => {
          findByIdEmailAndPasswordMethod = sinon.fake.throws('An error');
        });

        it('returns an error', async () => {
          const payload = { email: 'email@domain.com', password: '123' }

          const response = await makeRequest('post', '/users/login', payload);

          const expectedResponse = { errors: [{ type: 'internal_server_error', message: 'There was an error processing your request' }] };
          expect(JSON.parse(response.payload)).to.equal(expectedResponse);
          expect(response.statusCode).to.equal(500);
        });
      });
    });
  });
});

const { createServer } = require('./lib/server');
const usersServiceFactory = require('./lib/services/users');
const authenticationServiceFactory = require('./lib/services/authentication');

const mysqlConfig = {
  host: process.env.MYSQL_USERS_HOST,
  port: process.env.MYSQL_USERS_PORT,
  user: process.env.MYSQL_USERS_USERNAME,
  password: process.env.MYSQL_USERS_PASSWORD,
  database: process.env.MYSQL_USERS_DATABASE
}
const userService = usersServiceFactory.instantiate(mysqlConfig);
const authenticationService = authenticationServiceFactory.instantiate();
const services = { users: userService, authentication: authenticationService };

createServer(services).start();
